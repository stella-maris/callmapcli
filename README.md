# CallMap CLI

A tool to creat ean overview of your applications method-calls.

This tool generates a PlantUML file from a CallMap-JSON file.

## Installation

CallMap-CLI is best installed using composer

```bash
composer require --dev stella-maris/callmap-cli
```

## Usage

After you have created a callmap-JSON file (for example via the stella-maris/callmap
plugin for PHPStan) you can create the PlantUML file via this command:

```bash
./bin/callmap render <path/to/callmap.json>
```

This will generate a PlantUML file in the current directory names `callmap.plantuml`.

You can use this file to generate a PNG os SVG using a PlantUML renderer like at
http://www.plantuml.com/plantuml/uml/

For the `stella-maris/callmap` plugin that should generate something like this:

![Example output](img.png)


## Full chain:

```bash
# Install PHPStan
composer require --dev phpstan/phpstan 1.11.x-dev
# Install plugin
composer require --dev stella-maris/callmap dev-main
# parse the sources and generate the callmap.json file
./vendor/bin/phpstan analyse -c vendor/stella-maris/callmap/callmap.neon [path/to/your/sources]
# Install the callmap cli
composer require stella-maris/callmap-cli@dev-main
# Convert the callmap.json file into a callmap.plantuml file
./bin/callmap render callmap.json
# Render a PNG file from the callmap.plantuml file
docker run -v "$(pwd):/app" -w "/app" ghcr.io/plantuml/plantuml callmap.plantuml
```

When done in this folder it should create a PNG file like this:

![Callmap image of callmap-cli](callmap.png)
